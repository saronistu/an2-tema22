<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DateAndTimeController extends AbstractController
{
    /**
     * @Route("/datasiora", name="display")
     */
    public function displayDateAndTime() {
        date_default_timezone_set("Europe/Bucharest");
        return new Response(date("F j, Y, g:i a"));
    }
}
